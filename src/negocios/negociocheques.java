/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocios;

import datos.archivo;
import java.util.ArrayList;
import objetos.objetoscheques;

/**
 *
 * @author Usuario
 */
public class negociocheques {

    archivo infocheques = new archivo();

    public void InsertarPersonas(ArrayList<objetoscheques> listacChequeses) {
        String datos = "";
        for (int i = 0; i <= listacChequeses.size() - 1; i++) {
            String nombre = listacChequeses.get(i).getNombre();
            int cedula = listacChequeses.get(i).getCedula();
            int desay = listacChequeses.get(i).getPagoDesay();
            int almuerzo = listacChequeses.get(i).getPagoAlmuer();
            int cena = listacChequeses.get(i).getPagoCena();
            int hotel = listacChequeses.get(i).getPagoHote();
            int total = desay + almuerzo + cena + hotel + 10000;
            datos =  nombre + "," + cedula + "," + desay + "," + almuerzo + "," + cena + "," + hotel + "," + total;
        }
        infocheques.InsertarenArchivo(datos);

    }

    public String[][] extrae() {
        ArrayList<String> lista_personas = infocheques.LeerDesdeArchivo();
        String[][] datoMatriz1 = new String[lista_personas.size()][7];
        for (int i = 0; i < lista_personas.size(); i++) {
            String valor[] = lista_personas.get(i).split(",");
            for (int j = 0; j <= 6; j++) {
                datoMatriz1[i][j] = valor[j];
            }
        }
        return datoMatriz1;

    }
}
